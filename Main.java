import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {


        List<Samochody> samochody = new ArrayList<>();
        samochody.add(new Samochody("opel", "benzyna", 180));
        samochody.add(new Samochody("Toyota", "benzyna", 175));
        samochody.add(new Samochody("BMW", "gaz", 220));
        samochody.add(new Samochody("Nissan", "diesel", 210));
        samochody.add(new Samochody("Ferrari", "benzyna", 250));
        samochody.add(new Samochody("Mercedes", "diesel", 230));
        samochody.add(new Samochody("Skoda", "gaz", 195));
        samochody.add(new Samochody("STAR", "DISEL", 87));
        samochody.add(new Samochody("MOTOR�WKA", "benzyna", 287));
        samochody.add(new Samochody("volvo", "benzyna", 500)); //Piotrek



        for (Samochody samochod : samochody) {
            if (samochod.typSilnika == "benzyna") {
                samochod.informacjeOsamochodzie();
            }

        }
    }

}


